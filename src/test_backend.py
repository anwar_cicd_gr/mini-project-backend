'''These are the unit tests for our backend. They're to be run with `pytest`.
'''

import mysql.connector
from fastapi.testclient import TestClient

from backend import app

client = TestClient(app)


class PatchedCursor:
    '''This is a custom class that implements those parts of the cursor object
    from mysql.connector that the API uses (execute and fetchall). The
    functionality is replaced with mocks used for testing without the need of an
    actual MariaDB server.
    '''

    def execute(self, *args, **kwargs):
        '''This method does nothing.
        '''

    def fetchall(self):
        '''This method returns hardcoded data
        '''
        return ["This", "is", "mocked", "data"]


class PatchedConnection:
    '''This is a custom class that implements those parts of the connection
    object from mysql.connector that the API uses (cursor, is_connected, close).
    The functionality is replaced with mocks used for testing without the need
    of an actual MariaDB server.
    '''

    def cursor(self):
        '''This method returns a PatchedCursor instance.
        '''
        return PatchedCursor()

    def is_connected(self):
        '''This method returns always True.
        '''
        return True

    def close(self):
        '''This method does nothing.
        '''


class PatchedFailedConnection(PatchedConnection):
    '''This class inherits from PatchedConnection, but it is used to mock a
    failed connection instead. All the methods except is_connected are inherited
    from PatchedConnection.
    '''

    def is_connected(self):
        '''This method returns always False.
        '''
        return False


def new_connection(*args, **kwargs):
    '''This function returns a new PatchedConnection instance.
    '''
    # I need to do something with args and kwargs, otherwise pylint will
    # complain. So I'm just going to delete them.
    del args, kwargs

    return PatchedConnection()


def new_failed_connection(*args, **kwargs):
    '''This function returns a new PatchedFailedConnection instance.
    '''
    # I need to do something with args and kwargs, otherwise pylint will
    # complain. So I'm just going to delete them.
    del args, kwargs

    return PatchedFailedConnection()


def test_read_main(monkeypatch):
    '''This is our first unit test: it checks that the endpoint returns the
    right data in the right format, and that it returns status 200 when the
    connection to the db succeeds.
    '''
    monkeypatch.setattr(mysql.connector, "connect", new_connection)

    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"data": ["This", "is", "mocked", "data"]}


def test_fail_read_main(monkeypatch):
    '''This is our second unit test: it test that the endpoint actually returns
    a 500 status when the connection to the db fails.
    '''
    monkeypatch.setattr(mysql.connector, "connect", new_failed_connection)

    response = client.get("/")
    assert response.status_code == 500
